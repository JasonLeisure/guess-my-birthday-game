from random import randint

name = input ("Hello, what is your name? ")

#Guess 1

month_number = randint(1, 12)
year_number = randint(1800, 2022)

print("Guess 1 :", name, "were you born in",
    month_number, "/" , year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I am a good guesser!")
    exit()
else:
    print("Let me try again!")

# Guess 2

month_number = randint(1, 12)
year_number = randint(1800, 2022)

print("Guess 2 :", name, "were you born in",
    month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Let me try again!")

#Guess 3

month_number = randint(1, 12)
year_number = randint(1800, 2022)

print("Guess 3:", name, "were you born in",
    month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Let me try again!")

#Guess 4

month_number = randint(1, 12)
year_number = randint(1800, 2022)

print("Guess 4 :", name, "were you born in",
    month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Let me try again!")

#Guess 5

month_number = randint(1, 12)
year_number = randint(1800, 2022)

print("Guess 5 :", name, "were you born in",
    month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("I'm busy and done with this. Good bye.")
